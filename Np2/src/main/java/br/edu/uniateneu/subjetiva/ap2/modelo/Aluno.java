package br.edu.uniateneu.subjetiva.ap2.modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_aluno")
public class Aluno {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="cd_academico")
	private Long id;
	@Column(name="ds_nome")
	private String nome;
	@Column(name="ds_matricula")
	private String matriculo;
	@Column(name="ds_cpf")
	private String cpf;
	@Column(name="ds_rg")
	private String rg;
	@Column(name="ds_sexo")
	private String sexo;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="tb_curso_aluno",
	joinColumns={@JoinColumn(name="cd_curso")},
	inverseJoinColumns={@JoinColumn(name="cd_academico")})
	private List<Curso> cursos;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatriculo() {
		return matriculo;
	}
	public void setMatriculo(String matriculo) {
		this.matriculo = matriculo;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
}
	
	
	
	
	



