package br.edu.uniateneu.subjetiva.ap2.modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_curso")
public class Curso {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "cd_curso")
		private Long id;
		@Column(name = "ds_nome")
		private String nome;
		@Column(name = "qtd_semestres")
		private Integer semestre;
		@Column(name = "ds_tipo")
		private String tipo;
		
		
		
		
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}


		public String getNome() {
			return nome;
		}



		public void setNome(String nome) {
			this.nome = nome;
		}


		public Integer getSemestre() {
			return semestre;
		}

		public void setSemestre(Integer semestre) {
			this.semestre = semestre;
		}

		public String getTipo() {
			return tipo;
		}


		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public List<Aluno> getAlunos() {
			return alunos;
		}

		public void setAlunos(List<Aluno> alunos) {
			this.alunos = alunos;
		}

		@ManyToMany(mappedBy = "cursos", cascade = CascadeType.ALL)
		private List<Aluno> alunos;
	}



