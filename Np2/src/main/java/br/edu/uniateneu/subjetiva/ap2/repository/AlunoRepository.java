package br.edu.uniateneu.subjetiva.ap2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.subjetiva.ap2.modelo.Aluno;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Long>{

}
