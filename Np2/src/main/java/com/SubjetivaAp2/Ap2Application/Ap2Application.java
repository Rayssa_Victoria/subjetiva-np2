package com.SubjetivaAp2.Ap2Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
@EntityScan("br.edu.uniateneu.subjetiva.ap2.modelo*")
@SpringBootApplication
public class Ap2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ap2Application.class, args);
	}

}
