package br.edu.uniateneu.subjetiva.ap2.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.subjetiva.ap2.modelo.Curso;
import br.edu.uniateneu.subjetiva.ap2.modelo.ResponseModel;
import br.edu.uniateneu.subjetiva.ap2.repository.CursoRepository;

@RestController
@RequestMapping(value = "/cursos")
	public class CursoService {
		@Autowired
		CursoRepository cursoRepository;

		@Produces("application/json")
		@RequestMapping(value = "/list", method = RequestMethod.GET)
		public @ResponseBody ArrayList<Curso> retorna() {
			return (ArrayList<Curso>) cursoRepository.findAll();
		}

		@Consumes("application/json")
		@Produces("application/json")
		@RequestMapping(value = "/salvar", method = RequestMethod.POST)
		public @ResponseBody Curso salvar(@RequestBody Curso curso) {
			Curso user  = this.cursoRepository.save(curso);
			return user;
		}

		@Consumes("application/json")
		@Produces("application/json")
		@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
		public @ResponseBody Curso atualizar(@RequestBody Curso curso) {
			Curso user = this.cursoRepository.save(curso);
			return user;
		}

		@Produces("application/json")
		@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
		public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
			try {
			Curso aluno = cursoRepository.getReferenceById(codigo);
			System.out.println("********"+ aluno.getId());
			
			
			if (cursoRepository.findById(codigo).isEmpty()) {
				return new ResponseModel(500, "Aluno não encontrado.");
			}
			
				cursoRepository.delete(aluno);
				return new ResponseModel(200, "Registro excluido com sucesso!");

			} catch (Exception e) {
				return new ResponseModel(500, e.getMessage());
			}

		}
		
		
	}


